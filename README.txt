CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

-- INTRODUCTION --
------------------

This module helps to list all the displays of the view and the style format
that the display uses. For a new site, a developer don't know the views which 
uses particular style format. They have spend time to click on each view to know 
the style format. 

This module will help to filter the list of views and displays 
that uses the given Style format. This module will display in the Views page and 
you can go through by clicking the tab present on this page.


-- REQUIREMENTS --
------------------
1. Drupal 7 latest version
2. Views module latest version 

-- INSTALLATION --
------------------
Download and install the View and Style format list module as normal.

-- CONFIGURATION --
------------------
 
Configure 'View and Formats list' permissions in Administration » People »
Permissions

-- MAINTAINERS --
-----------------
 * Revathi Dinesh - https://www.drupal.org/u/revathidinesh-0
