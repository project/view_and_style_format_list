<?php

/**
 * @file
 * view_and_format_fetching.pages.inc
 */

/**
 * Implements hook_form().
 */
function view_and_format_list_get_format_of_views($form, &$form_state) {
  if (module_exists('views')) {
    $style_options = views_fetch_plugin_names('style', 'normal', array());
    array_unshift($style_options, '--None--');
    $load_all_views = view_and_format_list_get_all_views_name();
    $form['all_views_name'] = array(
      '#type' => 'select',
      '#title' => t('Select View') ,
      '#options' => $load_all_views ,
      '#weight' => 0,
    );
    $form['format_opt'] = array(
      '#type' => 'select',
      '#title' => t('Select Style Format') ,
      '#options' => $style_options ,
      '#weight' => 1,
    );
    $sample_data = view_and_format_list_get_style_plugin();
    $sample_header = array(
      'view_name' => t('View Name') ,
      'display_name' => t('Display Name') ,
      'style_format' => t('Style Format') ,
    );
    $options = array();
    if (isset($form_state['values'])&&(!empty($form_state['values']['format_opt'])||!empty($form_state['values']['all_views_name']))) {
      if (!empty($form_state['values']['format_opt'])&&!empty($form_state['values']['all_views_name'])) {
        if ($form_state['values']['format_opt'] == 'default') {
          $form_state['values']['format_opt'] = 'Unformatted list';
        }
        if ($form_state['values']['format_opt'] == 'list') {
          $form_state['values']['format_opt'] = 'HTML list';
        }
        if (view_and_format_list_in_array_r($form_state['values']['format_opt'], $sample_data) && view_and_format_list_in_array_r($form_state['values']['all_views_name'], $sample_data)) {
          foreach ($sample_data as $key => $data) {
            if (($data[2] == $form_state['values']['format_opt']) && ($data[3] == $form_state['values']['all_views_name'])) {
              $view_link_name = ucfirst($data[0]);
              $options[$key] = array(
                'view_name' => l($view_link_name, '/admin/structure/views/view/' . $data[5] . '/edit/' . $data[4]),
                'display_name' => ucfirst($data[1]) ,
                'style_format' => ucfirst($data[2])  ,
              );
            }
            else {
              continue;
            }
          }
        }
      }
      elseif (!empty($form_state['values']['format_opt'])) {
        if ($form_state['values']['format_opt'] == 'default') {
          $form_state['values']['format_opt'] = 'Unformatted list';
        }
        if ($form_state['values']['format_opt'] == 'list') {
          $form_state['values']['format_opt'] = 'HTML list';
        }
        if (view_and_format_list_in_array_r($form_state['values']['format_opt'], $sample_data)) {
          foreach ($sample_data as $key => $data) {
            if (($data[2] == $form_state['values']['format_opt'])) {
              $view_link_name = ucfirst($data[0]);
              $options[$key] = array(
                'view_name' => l($view_link_name, '/admin/structure/views/view/' . $data[5] . '/edit/' . $data[4]),
                'display_name' => ucfirst($data[1]) ,
                'style_format' => ucfirst($data[2])  ,
              );
            }
            else {
              continue;
            }
          }
        }
      }
      elseif (!empty($form_state['values']['all_views_name'])) {
        if (view_and_format_list_in_array_r($form_state['values']['all_views_name'], $sample_data)) {
          foreach ($sample_data as $key => $data) {
            if (($data[3] == $form_state['values']['all_views_name'])) {
              $view_link_name = ucfirst($data[0]);
              $options[$key] = array(
                'view_name' => l($view_link_name, '/admin/structure/views/view/' . $data[5] . '/edit/' . $data[4]),
                'display_name' => ucfirst($data[1]) ,
                'style_format' => ucfirst($data[2])  ,
              );
            }
            else {
              continue;
            }
          }
        }
      }
      else {
        drupal_set_message(t('No records found'));
      }
      if (!array_filter($options)) {
        drupal_set_message(t('No records found'));
      }
    }
    else {
      foreach ($sample_data as $key => $data) {
        $view_link_name = ucfirst($data[0]);
        $options[$key] = array(
          'view_name' => l($view_link_name, '/admin/structure/views/view/' . $data[5] . '/edit/' . $data[4]),
          'display_name' => ucfirst($data[1]) ,
          'style_format' => ucfirst($data[2])  ,
        );
      }
    }

    $form['actions'] = array(
      '#type' => 'actions' ,
      '#weight' => 3,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter') ,
    );
    $form['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset') ,
      '#submit' => array(
        'view_and_format_list_get_format_of_views_reset',
      ) ,

    );
    $form['views_format_table'] = array(
      '#theme' => 'table',
      '#header' => $sample_header,
      '#rows' => $options,
      '#weight' => 4,
    );
    return $form;
  }
  else {
    drupal_set_message(t("Views not enabled"));
  }
}

/**
 * Implements hook_form_submit().
 */
function view_and_format_list_get_format_of_views_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_form_reset().
 */
function view_and_format_list_get_format_of_views_reset($form, &$form_state) {
  $form_state['rebuild'] = FALSE;
}

/**
 * Function helps to get all the views.
 */
function view_and_format_list_get_all_views_name() {
  $all_views_array = array(
    '0' => '--None--' ,
  );
  $query = db_select('views_view', 'vv');
  $query->fields('vv', array(
    'vid',
    'human_name',
    'name',
  ));
  $result = $query->execute()->fetchAll();
  foreach ($result as $view_row) {
    if (!empty($view_row->human_name)) {
      $all_views_array[$view_row->vid] = $view_row->human_name;
    }
    else {
      $all_views_array[$view_row->vid] = $view_row->name;
    }
  }
  return $all_views_array;
}

/**
 * Function helps to get the style plugin of the given view.
 */
function view_and_format_list_get_style_plugin($vid = NULL, $style = NULL) {
  $views_formats = array();
  $query = db_select('views_view', 'vv');
  $query->fields('vv', array(
    'vid',
    'human_name',
    'name',
  ));
  $query->fields('vs', array(
    'id',
    'display_title',
    'display_plugin',
    'display_options',
  ));
  $query->join('views_display', 'vs', 'vs.vid=vv.vid');
  if (!is_null($vid) || !empty($vid)) {
    $query->condition('vv.vid', $vid, '=');
  }
  if (!is_null($style) || !empty($style)) {
    $query->condition('vs.display_options', '%' . $style . '%', 'LIKE');
  }
  $result = $query->execute()->fetchAll();
  foreach ($result as $key => $display_array) {
    if (!empty($display_array->human_name)) {
      $view_name = $display_array->human_name;
    }
    else {
      $view_name = $display_array->name;
    }
    $style_opt = unserialize($display_array->display_options);
    if (!empty($style_opt['style_plugin'])) {
      if ($style_opt['style_plugin'] == 'default') {
        $style_opt['style_plugin'] = 'Unformatted list';
      }
      if ($style_opt['style_plugin'] == 'list') {
        $style_opt['style_plugin'] = 'HTML list';
      }
    }
    if (empty($style_opt['style_plugin'])) {
      $style_opt['style_plugin'] = view_and_format_list_get_default_style_plugin_for_view($display_array->vid, $display_array->id);
    }
    $views_formats[$key] = array(
      $view_name,
      $display_array->display_title,
      str_replace('_', ' ', $style_opt['style_plugin']),
      $display_array->vid,
      $display_array->id,
      $display_array->name,
    );
  }

  return $views_formats;
}

/**
 * Function helps to get the default style plugin of the given view.
 */
function view_and_format_list_get_default_style_plugin_for_view($vid, $display) {
  $query = db_select('views_view', 'vv');
  $query->fields('vs', array(
    'display_options',
  ));
  $query->join('views_display', 'vs', 'vs.vid=vv.vid');
  $query->condition('vv.vid', $vid, '=');
  $query->condition('vs.id', 'default', '=');
  $result = $query->execute()->fetchField();
  $default_style_options = unserialize($result);
  return $default_style_options['style_plugin'];
}

/**
 * Function helps to check if the value present in the multidimensional array.
 */
function view_and_format_list_in_array_r($needle, $haystack, $strict = FALSE) {
  foreach ($haystack as $item) {
    if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && view_and_format_list_in_array_r($needle, $item, $strict))) {
      return TRUE;
    }
  }
  return FALSE;
}
